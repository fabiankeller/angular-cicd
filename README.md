# Angular CI/CD Toolkit

A docker image to build, test and deploy Angular applications from CI/CD.

The docker image contains the following tools preinstalled:

- Node & NPM
- Headless Chromium (`CHROME_PATH` and `CHROME_BIN` are there for you)
- Utilities for convenience: `jq` / `wget` / `curl` / `git`
- Deployment tools: `rsync` / `terraform`

You may use this from any CI/CD system. 
If you happen to be using GitLab CI/CD then the following `.gitlab-ci.yml` file gives you a headstart:

```
image: registry.gitlab.com/fabiankeller/angular-cicd:latest

build:
  stage: build
  before_script:
  - npm install
  artifacts:
    name: app-production
    paths:
    - dist/
    expire_in: 3 days
  script: |
    set -ex
    npm run build-production
    
test:
  stage: test
  before_script:
  - npm install
  script: |
    set -ex
    npm run test --headless
    npm run e2e
```