FROM node:12-alpine

ENV CHROME_PATH=/usr/lib/chromium
ENV CHROME_BIN=/usr/bin/chromium-browser

# install tools via apk
RUN apk update && \
    apk --no-cache --update add chromium \
    jq wget curl git \
    rsync terraform
    
# install aws cli
RUN apk --no-cache --update add python3 py3-pip py3-setuptools
RUN /usr/bin/pip3 install awscli && \
    aws --version

ENTRYPOINT []
CMD [ "bash" ]
